<?php

use Virta\Container;

require 'vendor/autoload.php';

$container = new Container(require 'config/service.php');
/** @var \Virta\Router $router */
$router = $container->resolve(\Virta\Router::class);

if($router instanceof \Virta\Router\HttpRouter) {
    $handler = $router->route(require 'config/routes.php');
} else {
    throw new Exception('Router is not handled');
}

/** @var \Virta\Action $action */
$action = $container->resolve($handler);

/** @var \Virta\Collector $collector */
$collector = $container->resolve(\Virta\Collector::class);

$response = $container->resolve(\Virta\Response::class);

$response->send($action->execute($collector->collect()));
