<?php

use Virta\Action\BuyCharge;
use Virta\Action\SaveCompany;

return [
    'PUT@company' => SaveCompany::class,
    'buy_charge' => BuyCharge::class
];
