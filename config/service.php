<?php

return [
    \Virta\Storage::class => \Virta\Storage\MySQLStorage::class,
    \Virta\Connection::class => new \Virta\Connection('root', '', 'virta'),
    \Virta\Router::class => \Virta\Router\HttpRouter::class,
    \Virta\Response::class => \Virta\Response\HttpResponse::class,
    \Virta\Collector::class => \Virta\Collector\HttpInputCollector::class,
    \Virta\InvalidAction::class => \Virta\Action\Lost::class
];
