<?php namespace Virta\Response;

use Virta\Response;

class HttpResponse implements Response
{
    public function send(array $data = [])
    {
        if((getallheaders()['Accept'] ?? false) === 'application/json') {
            header('Content-Type: application/json');
            echo json_encode($data);
        }

    }
}
