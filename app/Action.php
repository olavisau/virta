<?php namespace Virta;

interface Action
{
    public function execute(array $data = []);
}
