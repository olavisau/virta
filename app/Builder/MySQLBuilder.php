<?php namespace Virta\Builder;

class MySQLBuilder
{
    const LINE = "\n    ";
    const COMMA_LINE = ',' . self::LINE;

    public function insert(string $table, array $column_map): string
    {
        $column2valueString = $this->column2value($column_map);

        return "
    insert into $table set 
    {$column2valueString}
";
    }

    public function insertOrUpdate(string $table, array $column_map)
    {
        $insert             = $this->insert($table, $column_map);
        $column2valueString = $this->column2value($column_map);

        return "{$insert} on duplicate key update {$column2valueString}";
    }

    public function select(string $table)
    {
        return "select * from $table where id = :id";
    }

    public function delete(string $table)
    {
        return "delete from $table where id = :id";
    }


    public function column2value($column_map)
    {
        $set_parts = [];
        foreach ($column_map as $column_name => $column_definition) {
            if ( is_numeric($column_name) ) {
                $column = $column_definition;
            } else {
                $column = $column_name;
            }
            $set_parts[] = "$column = :$column_definition";
        }

        return self::LINE . implode(self::COMMA_LINE, $set_parts);
    }
}
