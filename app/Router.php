<?php namespace Virta;


interface Router
{
    public function route(array $routes, $route = null): string;
}
