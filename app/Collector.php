<?php namespace Virta;


interface Collector
{
    public function collect();
}
