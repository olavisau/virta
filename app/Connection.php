<?php namespace Virta;

use PDO;

class Connection {
    public function __construct($user, $password, $database, $host = 'localhost')
    {
        $this->pdo = new PDO("mysql:host=$host;dbname=$database;", $user, $password);
        $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
    }

    public function execute(string $query, array $bindings)
    {
        $statement = $this->pdo->prepare($query);

        $statement->execute($bindings);
        return $statement;
    }

    public function ID()
    {
        return $this->pdo->lastInsertId();
    }
}
