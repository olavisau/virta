<?php namespace Virta\Collector;


use Virta\Collector;

class HttpInputCollector implements Collector
{

    public function collect()
    {
        if((getallheaders()['Content-Type'] ?? false) === 'application/json') {
            return json_decode(
                file_get_contents('php://input')
            , true);
        }
        return [];
    }
}
