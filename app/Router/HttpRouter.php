<?php namespace Virta\Router;


use Virta\InvalidAction;
use Virta\Router;

class HttpRouter implements Router
{


    public function route(array $routes, $route = null): string
    {
        if(!$route) {
            $route = $this->detect();
        }

        foreach ($routes as $path => $action)
        {
            if($this->match($route, $path)) {
                return $action;
            };
        }

        return InvalidAction::class;
    }

    private function detect()
    {
        return $_SERVER['REQUEST_METHOD'] . '@' . trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/');
    }

    private function match(string $expected, string $current)
    {
        return $expected === $current;
    }
}
