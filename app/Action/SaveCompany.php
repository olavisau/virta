<?php namespace Virta\Action;


use Virta\Action;
use Virta\Storage;

class SaveCompany implements Action
{

    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }


    public function execute(array $data = [])
    {
        return [
            'id' => $this->storage->save('company', $data)
        ];
    }
}
