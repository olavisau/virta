<?php namespace Virta\Action;


use Virta\Action;
use Virta\Storage;

class Lost implements Action
{

    public function execute(array $data = [])
    {
        return [
            'message' => 'Requested resource not found'
        ];
    }
}
