<?php namespace Virta\Storage;

use Virta\Builder\MySQLBuilder;
use Virta\Connection;
use function array_keys as keys;

class MySQLStorage implements \Virta\Storage
{

    private $connection = null;
    private $builder = null;

    public function __construct(Connection $connection, MySQLBuilder $builder)
    {
        $this->connection = $connection;
        $this->builder = $builder;
    }

    public function save(string $container, array $item): int
    {
        $this->connection->execute(
            $this->builder->insertOrUpdate($container, keys($item)),
            $item
        );
        return $item['id'] ?? $this->connection->ID();
    }

    public function delete(string $container, int $id): void
    {
        $this->connection->execute($this->builder->delete($container), ['id' => $id]);
    }

    public function fetch(string $container, int $id) : ?array
    {
        return $this->connection->execute($this->builder->select($container), ['id' => $id])->fetch() ?: null;
    }
}
