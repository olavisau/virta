<?php namespace Virta;

interface Storage
{
    public function save(string $container, array $item): int;

    public function delete(string $container, int $id): void;

    public function fetch(string $container, int $id): ?array;
}
