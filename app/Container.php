<?php namespace Virta;



class Container
{

    private $reflection_cache = [];

    private $service_map;

    public function __construct(array $service_map)
    {
        $this->service_map = $service_map;

        $this->service_map[Container::class] = $this;

    }

    public function resolve(string $interface)
    {
        $class = $this->service($interface);

        $reflection = $this->reflect($class);

        if(!$constructor = $reflection->getConstructor()) {
            return new $class;
        }

        $arguments = [];

        foreach ($constructor->getParameters() as $parameter) {
            $interface = $parameter->getClass()->getName();

            $service = $this->service($interface);

            if($service instanceof $interface) {
                $arguments[] = $service;
            } else {
                $arguments[] = $this->resolve($interface);
            }
        }

        return $reflection->newInstanceArgs($arguments);
    }


    private function service(string $interface)
    {
        if(!$class = $this->service_map[$interface] ?? $interface) {
            throw new \Exception('Service is not registered');
        }
        return $class;
    }

    /**
     * @param string $class
     * @return \ReflectionClass
     */
    private function reflect(string $class)
    {
        if(empty($this->reflection_cache[$class])) {
            $this->reflection_cache[$class] = new \ReflectionClass($class);
        }
        return $this->reflection_cache[$class];
    }
}
