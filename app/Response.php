<?php namespace Virta;


interface Response
{
    public function send(array $data = []);
}
