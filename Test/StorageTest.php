<?php

use \PHPUnit\Framework\TestCase;
use Test\Config;
use Virta\Builder\MySQLBuilder;
use Virta\Connection;
use Virta\Storage\MySQLStorage;

class StorageTest extends TestCase
{
    public function testSave()
    {
        $storage = new MySQLStorage(
            new Connection(Config::USER, Config::PASSWORD, Config::DB),
            new MySQLBuilder()
        );

        $name = 'example name';
        $container = 'company';
        $id = $storage->save($container, [
            'name' => $name
        ]);

        $item = $storage->fetch($container, $id);

        $this->assertEquals($item['name'], $name);

        $storage->save($container, [
            'name' => $name,
            'id'   => $id
        ]);

        $item = $storage->fetch($container, $id);

        $this->assertEquals($item['name'], $name);

        $storage->delete($container, $id);

        $first_id = 1;

        $this->assertNull($storage->fetch($container, $first_id));


    }
}
